package rpgquests.parsers;

public abstract class CommandParser<T> {
	
	public abstract T parse(String string);

}
